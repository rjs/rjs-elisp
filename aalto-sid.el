;;; aalto-sid.el --- help in typing Aalto University student IDs

;; Copyright (C) 2001,2012 Riku Saikkonen

;; Author: Riku Saikkonen <rjs@netti.fi>
;; Version: 2.0
;; Keywords: convenience, local

;; This file is *NOT* part of GNU Emacs.
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;; 02110-1301 USA.

;;; Commentary:
;;;
;;; This package is an aid for typing student IDs in the formats used
;;; in Aalto University, Finland. A previous version of this package
;;; was named hut-sid.el after the former name Helsinki University of
;;; Technology, Finland. You can load this package in the normal way:
;;; either M-x load-library RET aalto-sid RET or (require 'aalto-sid)
;;; if it is in your `load-path', or M-x load-file RET
;;; /path/to/aalto-sid.el RET if not.
;;;
;;; One way to have this package load automatically is to copy
;;; aalto-sid.el to the directory ~/.elisp and then add the following
;;; lines to your ~/.emacs:
;;;   (add-to-list 'load-path "~/.elisp")
;;;   (require 'aalto-sid)
;;;
;;; The package is implemented as a buffer-local minor mode. The
;;; package does nothing by default (except creating a few functions
;;; and variables for Emacs Lisp): to enable or disable the mode, use
;;; M-x aalto-sid-mode RET when you are editing a file containing
;;; student IDs.
;;;
;;; When the mode is enabled ("AaltoSID" is displayed in the mode
;;; line), the checksums of any student IDs are checked for validity
;;; as they are typed: all student IDs in the file will be highlighted
;;; in green or red according to their validity, and the bell will
;;; ring after typing an invalid ID. (To have a visual bell instead of
;;; an audible one, M-x customize-variable RET visible-bell RET.)
;;;
;;; Also, C-c C-s will be bound to a function that searches forward
;;; for an invalid student ID. If you want to check previously written
;;; student IDs, either look at the colors visually, or go to the
;;; start of your file (M-<) and press C-c C-s repeatedly; Emacs will
;;; then stop at each invalid student ID or tell you that there are
;;; none.
;;;
;;; Even without enabling aalto-sid-mode, you can still use some of
;;; the functions manually, though it is more cumbersome: for example,
;;; M-x aalto-sid-search-forward-for-invalid RET instead of the above
;;; C-c C-s.
;;;
;;; Beeping after typing a student ID is implemented by rebinding the
;;; keys SPC, TAB, RET, :, ;, and comma; this may well break some of
;;; the more complex major modes, but works well in Fundamental and
;;; Text modes, which I presume are the most commonly used ones for
;;; files with student IDs. If you encounter problems with it, just
;;; disable this feature using M-x customize-variable RET
;;; aalto-sid-enable-electric-keys RET.
;;;
;;; Four student ID formats are supported: the six-number student IDs
;;; currently used at Aalto, and the formats previously used at the
;;; former institutions HUT, HSE (Helsinki School of Economics) and
;;; UIAH (University of Art and Design Helsinki). However, the
;;; HSE-format IDs have no checksum to validate, so they will always
;;; be displayed green. Examples:
;;;   valid: Aalto: 234562 HUT: 1234W 12345S HSE: K12345 UIAH: 1234561
;;; invalid: Aalto: 234567 HUT: 12345A UIAH: 1234567
;;;
;;; See the description of the mode (C-h m) and the aalto-sid-*
;;; commands for details. Most of the features are customizable: try
;;; M-x customize-group RET aalto-sid RET.

;;; Change Log
;;;
;;; Version 2.0 (January 5th, 2012)
;;;  * Package renamed from hut-sid.el to aalto-sid.el.
;;;  * Support for Aalto-format student IDs.
;;;  * Support for old HSE-format student IDs (although they don't have
;;;    a checksum to validate).
;;;  * Support for old UIAH-format student IDs.
;;;  * Support for coloring student IDs using font-lock.
;;;  * Customization for selecting which features to enable.
;;;  * Some code cleanup.
;;;
;;; Version 1.0 (January 20th, 2001)
;;;  * Initial release of hut-sid.el.


;;; Code:

(eval-when-compile (require 'easy-mmode))
(require 'easy-mmode)

;;; Customizable variables

(defgroup aalto-sid nil
  "Customization for the `aalto-sid-mode' minor mode."
  :group 'local)

(defcustom aalto-sid-enable-electric-keys t
  "If non-nil, enable electric keys for `aalto-sid-mode'.
Disable to remove beeping after typing invalid student IDs."
  :type 'boolean
  :group 'aalto-sid)

(defcustom aalto-sid-enable-colors t
  "If non-nil, enable font-lock coloring for `aalto-sid-mode'."
  :type 'boolean
  :group 'aalto-sid)

(defcustom aalto-sid-search-forward-key "\C-c\C-s"
  "Key to bind to `aalto-sid-search-forward-for-invalid'.
Set to nil to disable the key binding."
  :type '(choice (const :tag "disabled" nil)
                 (string :tag "key"))
  :group 'aalto-sid)

(defface aalto-sid-valid-face
  '((t (:foreground "darkgreen")))
  "Face used for valid student IDs in `aalto-sid-mode'."
  :group 'aalto-sid)

(defface aalto-sid-invalid-face
  '((t (:bold t :foreground "red")))
  "Face used for invalid student IDs in `aalto-sid-mode'."
  :group 'aalto-sid)

;;; Internal variables

;; If you change this to be less specific, you probably also need to
;; change `aalto-sid-validate' below: it is called to validate strings
;; that match this regexp. The assumption of exactly one word comes
;; from the (forward-word -1) calls in the code below.
(defvar aalto-sid-regexp 
  "\\<\\([0-9]\\{6,7\\}\\|[0-9]\\{4,5\\}[[:alpha:]]\\|[Kk][0-9]\\{5\\}\\)\\>"
  "Regular expression that matches a student ID.
Only strings that match this regexp will be checked for validity.
The implementation assumes that the regexp matches exactly one
word.")

(defvar aalto-sid-font-lock-keywords
  `((,aalto-sid-regexp 0 (aalto-sid-face-function) t))
  "Font lock keywords for coloring student IDs.
`aalto-sid-activate-colors' adds these using
`font-lock-add-keywords'.")

;;; Internal functions

(defun aalto-sid-validate-hut-format (str)
  "Check validity of the HUT-format student ID at start of STR."
  (let* ((case-fold-search t)
         (i (string-match "[[:alpha:]]" str))
         (sid-num (string-to-number (substring str 0 i)))
         (sid-char (string-to-char (substring str i)))
         (calc-char
          (string-to-char (substring "ABCDEFHJKLMNPRSTUVW"
                                     (mod sid-num 19)))))
    (char-equal sid-char calc-char)))

(defun aalto-sid-validate-aalto-format (str)
  "Check validity of the Aalto-format student ID at start of STR.
Actually also validates the old UIAH-format student IDs."
  (let* ((i (string-match "[^0-9]" str))
         (sid-num (string-to-number (substring str 0 i)))
         (lastd (mod sid-num 10))
         (n (/ sid-num 10))
         (sum 0)
         (factors '#0=(7 3 1 . #0#)))
    (while (> n 0)
      (setq sum (+ sum (* (car factors) (mod n 10)))
            n (/ n 10)
            factors (cdr factors)))
    (setq sum (mod (- 10 (mod sum 10)) 10))
    (= sum lastd)))

(defun aalto-sid-validate (str)
  "Is STR a valid student ID?
STR is assumed to match `aalto-sid-regexp'." 
  (save-match-data
    (if (string-match "^[Kk]" str)     ; HSE-format student ID
        t                              ; which doesn't have a checksum
      (let ((i (string-match "[^0-9]" str)))
        (if (and i (< i 6))         ; less than 6 numers => HUT-format
            (aalto-sid-validate-hut-format str)
          ;; otherwise either Aalto- or UIAH-format
          (aalto-sid-validate-aalto-format str))))))

(defun aalto-sid-validate-before-point ()
  "Is the student ID before point valid?
The word before point is assumed to match `aalto-sid-regexp'."
  (let ((sid (buffer-substring (save-excursion (forward-word -1) (point))
                               (point))))
    (aalto-sid-validate sid)))

(defun aalto-sid-error-if-invalid (str)
  "Assert that STR is a valid student ID.
Calls `error' if it is not valid; otherwise, does nothing."
  (unless (aalto-sid-validate str)
    (error (concat "Student ID `" str "' invalid!"))))

(defun aalto-sid-face-function ()
  "Find the face of a student ID before point."
  (if (aalto-sid-validate-before-point)
      'aalto-sid-valid-face
    'aalto-sid-invalid-face))

;;; Searching commands

;; Used by the aalto-sid-electric-* functions.
(defun aalto-sid-beeping-check-before-point ()
  "Check the student ID before point, beeping if invalid.
Does nothing if the check succeeds or there is no student ID
before point. Gives a message and beeps if it finds an
invalid student ID."
  (interactive)
  (save-match-data
    (let ((sid (buffer-substring (save-excursion (forward-word -1) (point))
                                 (point))))
      (when (and (string-match (concat "\\`" aalto-sid-regexp "\\'") sid)
                 (not (aalto-sid-validate sid)))
        (message (concat "Student id `" sid "' invalid!"))
        (beep)))))

;; This is not bound to anything by default. I'm not sure if it's
;; useful, but left it here anyway...
(defun aalto-sid-check-at-bol ()
  "Check the student ID at the beginning of the current line.
The check is done silently, i.e., nothing happens if it succeeds.
Calls `error' if the check fails."
  (interactive)
  (save-excursion
    (save-match-data
      (let ((firstword
             (buffer-substring (progn (beginning-of-line-text) (point))
                               (progn (forward-word 1) (point)))))
        (if (string-match (concat "\\`" aalto-sid-regexp "\\'") firstword)
            (aalto-sid-error-if-invalid firstword))))))

(defun aalto-sid-search-forward-for-invalid ()
  "Search forward for an invalid student ID.
The search does not wrap around.
Calls `error' if the check fails."
  (interactive)
  (let ((orig-point (point)))
    (save-match-data
      (while (re-search-forward aalto-sid-regexp nil t)
        (aalto-sid-error-if-invalid (match-string 0))))
    (goto-char orig-point)
    (message "No invalid student IDs found.")))

;;; Activate and deactivate colors

(defun aalto-sid-activate-colors ()
  "Activate colored student IDs for `aalto-sid-mode'
for the current buffer. You should also enable `font-lock-mode'."
  (interactive)
  (font-lock-add-keywords nil aalto-sid-font-lock-keywords t))

(defun aalto-sid-deactivate-colors ()
  "Reverse the effect of `aalto-sid-activate-colors'."
  (interactive)
  (font-lock-remove-keywords nil aalto-sid-font-lock-keywords))

;;; Functions for electric characters
;;;
;;; These will override definitions from the major mode, so we hope
;;; there are none... (This package is mostly used in Text and
;;; Fundamental modes, I think.) Please tell me if you know of a better
;;; way to make "electric keys" in minor modes.

(defun aalto-sid-electric-character ()
  "Electric character for `aalto-sid-mode'.
Calls `aalto-sid-beeping-check-before-point' and
`self-insert-command'."
  (interactive)
  (aalto-sid-beeping-check-before-point)
  (self-insert-command 1))

(defun aalto-sid-electric-newline ()
  "Electric newline for `aalto-sid-mode'.
Calls `aalto-sid-beeping-check-before-point' and
`newline'."
  (interactive)
  (aalto-sid-beeping-check-before-point)
  (newline))

(defun aalto-sid-electric-tab ()
  "Electric TAB for `aalto-sid-mode'.
Calls `aalto-sid-beeping-check-before-point' and
`indent-for-tab-command'."
  (interactive)
  (aalto-sid-beeping-check-before-point)
  (indent-for-tab-command))

;;; Define the minor mode

(easy-mmode-define-minor-mode aalto-sid-mode
  "Toggle aalto-sid mode.
With no argument, this command toggles the mode.
Non-null prefix argument turns on the mode.
Null prefix argument turns off the mode.

When aalto-sid mode is enabled (as shown by a AaltoSID indicator
in the mode line), student IDs are checked after `:', `,', SPC,
TAB or RET is typed after them. If the student ID is invalid,
Emacs will beep. In addition, valid and invalid student IDs will
be colored according to `aalto-sid-valid-face' and
`aalto-sid-invalid-face'. These features can be disabled: see the
variables `aalto-sid-enable-electric-keys' and
`aalto-sid-enable-colors', or M-x customize-group RET aalto-sid
RET.

Also, by default C-c C-s is bound to
`aalto-sid-search-for-invalid', which searches forward for an
invalid student ID. See `aalto-sid-search-forward-key' to change
this default."
  nil
  " AaltoSID"
  `(,@(and aalto-sid-search-forward-key
           `((,aalto-sid-search-forward-key
              . aalto-sid-search-forward-for-invalid)))
    ,@(and aalto-sid-enable-electric-keys
           '((" " . aalto-sid-electric-character)
             (":" . aalto-sid-electric-character)
             (";" . aalto-sid-electric-character)
             ("," . aalto-sid-electric-character)
             ("\r" . aalto-sid-electric-newline)
             ("\t" . aalto-sid-electric-tab))))
  (when aalto-sid-mode
    (when (and aalto-sid-enable-colors font-lock-mode)
      (aalto-sid-activate-colors)
      (font-lock-fontify-buffer)))
  (unless aalto-sid-mode
    (if (and aalto-sid-enable-colors font-lock-mode)
        (aalto-sid-deactivate-colors))))

(provide 'aalto-sid)

;;; aalto-sid.el ends here
